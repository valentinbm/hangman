﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Helpers
{
    class Utils
    {
        public static string ConcatString(string first, string second, int numberOfTimes)
        {
            for (int i = 0; i < numberOfTimes; i++)
            {
                first += second;
            }

            return first;
        }

        public static string FindLetter(string word, string letter, string toMark, int indexMultiplierMark = 1)
        {
            word = word.ToUpper();
            char charLetter;

            if(!Char.TryParse(letter, out charLetter))
            {
                return null;
            }

            if (!word.Contains(charLetter))
            {
                return String.Empty;
            }

            int index = word.IndexOf(charLetter);
            char[] toMarkArray = toMark.ToCharArray();

            while (index != -1)
            {
                toMarkArray[index * indexMultiplierMark] = charLetter;
                if (index + 1 >= word.Length) break;
                index = word.IndexOf(charLetter, index + 1);
            }

            string markedString = String.Empty;

            for(int i = 0; i < toMarkArray.Length; i++)
            {
                markedString += toMarkArray[i];
            }

            return markedString;
        }
    }
}
