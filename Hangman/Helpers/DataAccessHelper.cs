﻿using Hangman.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Helpers
{
    class DataAccessHelper
    {
        public static ObservableCollection<User> GetUsersFromFile(string filePath)
        {
            ObservableCollection<User> users = new ObservableCollection<User>();
            
            string[] lines = File.ReadAllLines(filePath);

            foreach (string line in lines)
            {
                string[] subStrings = line.Split(' ');

                string userName = subStrings[0];
                string avatarPath = subStrings[1];

                users.Add(new User { Name = userName, AvatarPath = ResourcePathHelper.GetFullPath(@avatarPath) });
            }

            return users;
        }

        public static void WriteUsersToFile(string filePath, ObservableCollection<User> users)
        {
            string[] lines = new string[users.Count];

            int index = 0;

            foreach (User user in users)
            {
                string userName = user.Name;

                string imgAbsolutePath = user.AvatarPath;
                string imgName = Path.GetFileName(imgAbsolutePath);
                string imagesRelativeDirectoryPath = @"\Resources\Images\Avatars\";
                string imgRelativePath = imagesRelativeDirectoryPath + imgName;

                lines[index] = userName + ' ' + imgRelativePath;
                index++;
            }

            File.WriteAllLines(filePath, lines);
        }

        public static List<string> GetAllWords(string filePath)
        {
            List<String> words = File.ReadAllLines(filePath).ToList();
            return words;
        }

    }
}
