﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Helpers
{
    class ResourcePathHelper
    {
        public static string GetFullPath(string relativePath)
        {
            string workingDirectory = Environment.CurrentDirectory;
            string projectDirectory = System.IO.Directory.GetParent(workingDirectory).Parent.FullName;

            string fullPath = projectDirectory + relativePath;

            return fullPath;
        }

        public static string GetPathOfFutureDisplayedAvatar(string currentAvatarPath, bool nextAvatar)
        {
            string imgDirectoryPath = GetFullPath(@"\Resources\Images\Avatars");
            string imgNamePattern = "*.png";

            List<string> imagesPathList = Directory.GetFiles(imgDirectoryPath, imgNamePattern).ToList();

            currentAvatarPath = GetFullPath(@"\Resources\Images\Avatars\" + Path.GetFileName(currentAvatarPath));

            int index = imagesPathList.IndexOf(currentAvatarPath);

            if (index == -1)
            {
                return null;
            }

            if (!nextAvatar)
            {
                if (index != 0)
                {
                    return imagesPathList[--index];
                }

                return imagesPathList.Last();
            }

            if (index != imagesPathList.Count - 1)
            {
                return imagesPathList[++index];
            }

            return imagesPathList.First();       
        }

        public static string GetHangmanPath(int mistakesNr)
        {
            string imgDirectoryPath = GetFullPath(@"\Resources\Images\Hangman");
            string imgNamePattern = "*.jpg";

            List<string> imagesPathList = Directory.GetFiles(imgDirectoryPath, imgNamePattern).ToList();

            foreach(string path in imagesPathList)
            {
                if(Path.GetFileNameWithoutExtension(path) == mistakesNr.ToString())
                {
                    return path;
                }
            }

            return imagesPathList[0];
        }
    }
}
