﻿using Hangman.Helpers;
using Hangman.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Model
{
    class User : BasePropertyChanged
    {
        private string avatarPath = ResourcePathHelper.GetFullPath(@"\Resources\Images\1.png");
        public string Name { get; set; } = String.Empty;
        public string AvatarPath
        {
            get
            {
                return avatarPath;
            }
            set
            {
                this.avatarPath = value;
                NotifyPropertyChanged("AvatarPath");
            }
        }


    }

}
