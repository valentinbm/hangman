﻿using Hangman.Helpers;
using Hangman.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.Models
{
    class Game : BasePropertyChanged
    {
        private User user;
        public User GameUser => user;

        private int mistakesNr = 0;

        private string hangmanStatusPath = ResourcePathHelper.GetHangmanPath(0);
        public string HangmanStatusPath
        {
            get
            {
                return hangmanStatusPath;
            }
            set
            {
                this.hangmanStatusPath = value;
                NotifyPropertyChanged("HangmanStatusPath");
            }
        }

        public bool IsFinished { get; set; }
        public bool IsWin { get; set; }
        public Game(User user)
        {
            this.user = user;
        }

        public void AddMistake()
        {
            mistakesNr++;
            if(mistakesNr > 10)
            {
                IsFinished = true;
                IsWin = false;
            }
            HangmanStatusPath = ResourcePathHelper.GetHangmanPath(mistakesNr);
        }
    }
}
