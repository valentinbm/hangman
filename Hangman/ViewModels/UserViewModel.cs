﻿using Hangman.Helpers;
using Hangman.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.ViewModels
{
    class UserViewModel
    {
        private ObservableCollection<User> users;
        public ObservableCollection<User> Users => users;

        public UserViewModel()
        {          
            users = DataAccessHelper.GetUsersFromFile(ResourcePathHelper.GetFullPath(@"\Resources\Files\users.txt"));                           
        }                                  

        public void SaveUsers()
        {
            DataAccessHelper.WriteUsersToFile(ResourcePathHelper.GetFullPath(@"\Resources\Files\users.txt"), Users);
        }
    }
}
