﻿using Hangman.Helpers;
using Hangman.Model;
using Hangman.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman.ViewModels
{
    class GameViewModel
    {
        private Game game;
        public Game CurrentGame => game;

        private List<string> words;
        public List<string> Words => words;

        private int currentWordIndex = 0;
        public String CurrentWord => Words[currentWordIndex];

        public GameViewModel(Game game)
        {
            this.game = game;
            this.words = DataAccessHelper.GetAllWords(ResourcePathHelper.GetFullPath(@"\Resources\Files\words.txt"));
        }
        public void NewGame()
        {
            this.game = new Game(CurrentGame.GameUser);

            if (currentWordIndex + 1 > Words.Count)
            {
                currentWordIndex = 0;
                return;
            }
            currentWordIndex++;
        }

    }
}
