﻿using Hangman.Helpers;
using Hangman.Model;
using Hangman.Models;
using Hangman.ViewModels;
using Hangman.Views;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;


namespace Hangman
{
    /// <summary>
    /// Interaction logic for Authentification.xaml
    /// </summary>
    public partial class AuthentificationView : UserControl
    {
        public AuthentificationView()
        {
            InitializeComponent();
            DataContext = new UserViewModel();
        }

        private void btnNewUser_Click(object sender, RoutedEventArgs e)
        {
            Window newUserWindow = new Window
            {
                Title = "New user",
                Content = new NewUserView(),
                SizeToContent = SizeToContent.WidthAndHeight,
                ResizeMode = ResizeMode.NoResize
            };

            newUserWindow.ShowDialog();

        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Window window = Window.GetWindow(this);
            window.Close();
        }
        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            UserViewModel userVM = (DataContext as UserViewModel);
            userVM.SaveUsers();

            Window gameWindow = new Window
            {
                Title = "Hangman",
                Content = new GameView(),
                SizeToContent = SizeToContent.Manual,
                ResizeMode = ResizeMode.NoResize
            };

            (gameWindow.Content as GameView).DataContext = new GameViewModel(new Game(listBoxUsers.SelectedItem as User));
            Window.GetWindow(this).Close();

            gameWindow.ShowDialog();


        }

        private void setImgAvatarSource(string path)
        {
            try
            {
                imgAvatar.Source = new BitmapImage(new Uri(path, UriKind.Absolute));
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Error : Invalid image path format");
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Error : File {path} not found.");
            }
        }

        private void changeAvatar(bool next)
        {
            string futureAvatarPath = ResourcePathHelper.GetPathOfFutureDisplayedAvatar(imgAvatar.Source.ToString(), next);

            if (!String.IsNullOrEmpty(futureAvatarPath))
            {
                setImgAvatarSource(futureAvatarPath);
                (listBoxUsers.SelectedItem as User).AvatarPath = futureAvatarPath;
            }
        }

        private void btnPreviousAvatar_Click(object sender, RoutedEventArgs e)
        {
            changeAvatar(false);
        }

        private void btnNextAvatar_Click(object sender, RoutedEventArgs e)
        {
            changeAvatar(true);
        }

        private void setUserButtonsState(bool state)
        {
            btnPlay.IsEnabled = state;
            btnDeleteUser.IsEnabled = state;
            btnNextAvatar.IsEnabled = state;
            btnPreviousAvatar.IsEnabled = state;
        }

        private void setImgAvatarSourceToSelectedUser()
        {
            User selectedUser = listBoxUsers.SelectedItem as User;
            setImgAvatarSource(selectedUser.AvatarPath);
        }

        private void listBoxUsers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (listBoxUsers.SelectedItem == null)
            {
                setUserButtonsState(false);
                return;
            }

            setUserButtonsState(true);

            setImgAvatarSourceToSelectedUser();
        }

        private void btnDeleteUser_Click(object sender, RoutedEventArgs e)
        {
            UserViewModel userVM = this.DataContext as UserViewModel;
            userVM.Users.Remove(listBoxUsers.SelectedItem as User);
        }
    }
}
