﻿using Hangman.Helpers;
using Hangman.Models;
using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hangman.Views
{
    /// <summary>
    /// Interaction logic for GameView.xaml
    /// </summary>
    public partial class GameView : UserControl
    {
        public GameView()
        {
            InitializeComponent();
        }

        private void lblUsername_Loaded(object sender, RoutedEventArgs e)
        {
            this.lblUsername.Content = (this.DataContext as GameViewModel).CurrentGame.GameUser.Name;
        }

        private void imgStatus_Loaded(object sender, RoutedEventArgs e)
        {
            Binding myBinding = new Binding("HangmanStatusPath");
            GameViewModel gameVM = this.DataContext as GameViewModel;
            myBinding.Source = gameVM.CurrentGame;
            imgStatus.SetBinding(Image.SourceProperty, myBinding);         
        }

        private void imgAvatar_Loaded(object sender, RoutedEventArgs e)
        {
            string path = (this.DataContext as GameViewModel).CurrentGame.GameUser.AvatarPath;
            this.imgAvatar.Source = new BitmapImage(new Uri(path, UriKind.Absolute));
        }

        private void btnLetter_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            string letter = btn.Content.ToString();

            GameViewModel gameVM = this.DataContext as GameViewModel;
            string markedString = Utils.FindLetter(gameVM.CurrentWord, letter, txtToFind.Text, 3);

            if (String.IsNullOrEmpty(markedString))
            {
                gameVM.CurrentGame.AddMistake();
                if(gameVM.CurrentGame.IsFinished && !gameVM.CurrentGame.IsWin)
                {
                    MessageBox.Show("You lost! You cannot go to the next word until you guess the current one.");
                    setTxtBlocks();
                    enableLetterButtons();
                    return;
                }
                btn.IsEnabled = false;
                return;
            }
            txtToFind.Text = markedString;

            if(!txtToFind.Text.Contains("   "))
            {
                gameVM.CurrentGame.IsFinished = true;
                gameVM.CurrentGame.IsWin = true;
                MessageBox.Show("You won! Now you can guess the next word.");
                gameVM.NewGame();
                enableLetterButtons();
                setTxtBlocks();
            }
        }

        private void enableLetterButtons()
        {
            btn_a.IsEnabled = true;
            btn_b.IsEnabled = true;
            btn_c.IsEnabled = true;
            btn_d.IsEnabled = true;
            btn_e.IsEnabled = true;
            btn_f.IsEnabled = true;
            btn_g.IsEnabled = true;
            btn_h.IsEnabled = true;
            btn_i.IsEnabled = true;
            btn_j.IsEnabled = true;
            btn_k.IsEnabled = true;
            btn_l.IsEnabled = true;
            btn_m.IsEnabled = true;
            btn_n.IsEnabled = true;
            btn_o.IsEnabled = true;
            btn_p.IsEnabled = true;
            btn_q.IsEnabled = true;
            btn_r.IsEnabled = true;
            btn_s.IsEnabled = true;
            btn_t.IsEnabled = true;
            btn_u.IsEnabled = true;
            btn_v.IsEnabled = true;
            btn_x.IsEnabled = true;
            btn_y.IsEnabled = true;
            btn_z.IsEnabled = true;

        }

        private void setTxtBlocks()
        {
            GameViewModel gameVM = this.DataContext as GameViewModel;
            txtLinesToFill.Text = Utils.ConcatString("_", "  _", gameVM.CurrentWord.Length - 1);
            txtToFind.Text = Utils.ConcatString(" ", "   ", gameVM.CurrentWord.Length - 1);
        }
        private void txtToFill_Loaded(object sender, RoutedEventArgs e)
        {
            setTxtBlocks();
        }

        private void txtToFind_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
