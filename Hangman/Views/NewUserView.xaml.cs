﻿using Hangman.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hangman.Views
{
    /// <summary>
    /// Interaction logic for NewUserView.xaml
    /// </summary>
    public partial class NewUserView : UserControl
    {
        public NewUserView()
        {
            InitializeComponent();
            txtUsername.Focus();
        }

        private void executeAddUser()
        {
            if (String.IsNullOrEmpty(txtUsername.Text))
            {
                return;
            }

            Window mainWindow = Application.Current.MainWindow;
            AuthentificationView authentificationView = mainWindow.Content as AuthentificationView;
            UserViewModel userVM = authentificationView.DataContext as UserViewModel;

            userVM.Users.Add(new Model.User { Name = txtUsername.Text });

            Window.GetWindow(this).Close();
        }

        private void btnAddUser_Click(object sender, RoutedEventArgs e)
        {
            executeAddUser();
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.IsKeyDown(Key.Return))
            {
                executeAddUser();
            }
        }
    }
}
