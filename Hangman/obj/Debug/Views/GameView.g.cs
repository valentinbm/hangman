﻿#pragma checksum "..\..\..\Views\GameView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "77138D3E62C23454B2C18AB6C5E009EC8F7F144A42712DF3E463B069C86E4E1C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Hangman.ViewModels;
using Hangman.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Hangman.Views {
    
    
    /// <summary>
    /// GameView
    /// </summary>
    public partial class GameView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 18 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_New_Game;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_Open_Game;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_Save_Game;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_Statistics;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_Exit;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_All_Categories;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_Movies;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_Rivers;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_Cars;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_Mountains;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_Celebrities;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.MenuItem MI_About;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_a;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_b;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_c;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_d;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_e;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_f;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_g;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_h;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_i;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_j;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_k;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_l;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_m;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_n;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_o;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_p;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_q;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_r;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_s;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_t;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_u;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_v;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_w;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_x;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_y;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn_z;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgStatus;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblUsername;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgAvatar;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtLinesToFill;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\Views\GameView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtToFind;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Hangman;component/views/gameview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\GameView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MI_New_Game = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 2:
            this.MI_Open_Game = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 3:
            this.MI_Save_Game = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 4:
            this.MI_Statistics = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 5:
            this.MI_Exit = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 6:
            this.MI_All_Categories = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 7:
            this.MI_Movies = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 8:
            this.MI_Rivers = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 9:
            this.MI_Cars = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 10:
            this.MI_Mountains = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 11:
            this.MI_Celebrities = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 12:
            this.MI_About = ((System.Windows.Controls.MenuItem)(target));
            return;
            case 13:
            this.btn_a = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\..\Views\GameView.xaml"
            this.btn_a.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 14:
            this.btn_b = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\..\Views\GameView.xaml"
            this.btn_b.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btn_c = ((System.Windows.Controls.Button)(target));
            
            #line 46 "..\..\..\Views\GameView.xaml"
            this.btn_c.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.btn_d = ((System.Windows.Controls.Button)(target));
            
            #line 47 "..\..\..\Views\GameView.xaml"
            this.btn_d.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.btn_e = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\..\Views\GameView.xaml"
            this.btn_e.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btn_f = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\..\Views\GameView.xaml"
            this.btn_f.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.btn_g = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\..\Views\GameView.xaml"
            this.btn_g.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.btn_h = ((System.Windows.Controls.Button)(target));
            
            #line 51 "..\..\..\Views\GameView.xaml"
            this.btn_h.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.btn_i = ((System.Windows.Controls.Button)(target));
            
            #line 52 "..\..\..\Views\GameView.xaml"
            this.btn_i.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.btn_j = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\..\Views\GameView.xaml"
            this.btn_j.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.btn_k = ((System.Windows.Controls.Button)(target));
            
            #line 54 "..\..\..\Views\GameView.xaml"
            this.btn_k.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 24:
            this.btn_l = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\..\Views\GameView.xaml"
            this.btn_l.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 25:
            this.btn_m = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\..\Views\GameView.xaml"
            this.btn_m.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 26:
            this.btn_n = ((System.Windows.Controls.Button)(target));
            
            #line 57 "..\..\..\Views\GameView.xaml"
            this.btn_n.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 27:
            this.btn_o = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\..\Views\GameView.xaml"
            this.btn_o.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 28:
            this.btn_p = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\..\Views\GameView.xaml"
            this.btn_p.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.btn_q = ((System.Windows.Controls.Button)(target));
            
            #line 60 "..\..\..\Views\GameView.xaml"
            this.btn_q.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 30:
            this.btn_r = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\..\Views\GameView.xaml"
            this.btn_r.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 31:
            this.btn_s = ((System.Windows.Controls.Button)(target));
            
            #line 62 "..\..\..\Views\GameView.xaml"
            this.btn_s.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 32:
            this.btn_t = ((System.Windows.Controls.Button)(target));
            
            #line 63 "..\..\..\Views\GameView.xaml"
            this.btn_t.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            this.btn_u = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\..\Views\GameView.xaml"
            this.btn_u.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 34:
            this.btn_v = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\..\Views\GameView.xaml"
            this.btn_v.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 35:
            this.btn_w = ((System.Windows.Controls.Button)(target));
            
            #line 66 "..\..\..\Views\GameView.xaml"
            this.btn_w.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 36:
            this.btn_x = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\..\Views\GameView.xaml"
            this.btn_x.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 37:
            this.btn_y = ((System.Windows.Controls.Button)(target));
            
            #line 68 "..\..\..\Views\GameView.xaml"
            this.btn_y.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 38:
            this.btn_z = ((System.Windows.Controls.Button)(target));
            
            #line 69 "..\..\..\Views\GameView.xaml"
            this.btn_z.Click += new System.Windows.RoutedEventHandler(this.btnLetter_Click);
            
            #line default
            #line hidden
            return;
            case 39:
            this.imgStatus = ((System.Windows.Controls.Image)(target));
            
            #line 71 "..\..\..\Views\GameView.xaml"
            this.imgStatus.Loaded += new System.Windows.RoutedEventHandler(this.imgStatus_Loaded);
            
            #line default
            #line hidden
            return;
            case 40:
            this.lblUsername = ((System.Windows.Controls.Label)(target));
            
            #line 72 "..\..\..\Views\GameView.xaml"
            this.lblUsername.Loaded += new System.Windows.RoutedEventHandler(this.lblUsername_Loaded);
            
            #line default
            #line hidden
            return;
            case 41:
            this.imgAvatar = ((System.Windows.Controls.Image)(target));
            
            #line 73 "..\..\..\Views\GameView.xaml"
            this.imgAvatar.Loaded += new System.Windows.RoutedEventHandler(this.imgAvatar_Loaded);
            
            #line default
            #line hidden
            return;
            case 42:
            this.txtLinesToFill = ((System.Windows.Controls.TextBlock)(target));
            
            #line 74 "..\..\..\Views\GameView.xaml"
            this.txtLinesToFill.Loaded += new System.Windows.RoutedEventHandler(this.txtToFill_Loaded);
            
            #line default
            #line hidden
            return;
            case 43:
            this.txtToFind = ((System.Windows.Controls.TextBlock)(target));
            
            #line 75 "..\..\..\Views\GameView.xaml"
            this.txtToFind.Loaded += new System.Windows.RoutedEventHandler(this.txtToFind_Loaded);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

